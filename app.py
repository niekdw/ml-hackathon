import sys
from flask import Flask
from flask import jsonify
from flask_restful import Resource, Api, reqparse 
from sklearn.externals import joblib
import pandas as pd 

app = Flask(__name__)
api = Api(app)

string_cols = ['sample_uuid']
int_cols = ['body_length', 'user_age', 'user_type', 'channels']

parser = reqparse.RequestParser()
for col in string_cols:
    parser.add_argument(col, type=str)
for col in int_cols:
    parser.add_argument(col, type=int)

class Predict(Resource): 
    def __init__(self, *args, **kwargs):
        super().__init__(*args, **kwargs)
        self.model = self.load_model()

    def get(self):
        arguments = parser.parse_args() 

        body_length = arguments['body_length']
        user_age = arguments['user_age']
        user_type = arguments['user_type']
        channels = arguments['channels']
        
        test_df = pd.DataFrame(columns=['body_length','user_age','user_type','channels'])
        test = pd.DataFrame(columns=['body_length','user_age','user_type','channels'])
        test['body_length'] = pd.DataFrame([body_length])
        test['user_age'] = pd.DataFrame([user_age])
        test['user_type'] = pd.DataFrame([user_type])
        test['channels'] = pd.DataFrame([channels])
                            
        test_df = test_df.append(test)


        clf = joblib.load('code/rf-1.pkl') 
        predicted_result = clf.predict(test_df)


        #TODO
        prob = clf.predict_proba(test_df)

        if predicted_result is 1:
            probab = prob[0][1]
        else:
            probab = prob[0][0]
  
        result = { 
            'sample_uuid': arguments['sample_uuid'],
            'probability': float(probab), 
            'label': float(predicted_result) 
        }
        return jsonify(**result)

    def load_model(self):
        return None 
 
api.add_resource(Predict, '/api/v1/predict')

if __name__ == '__main__':
    app.run(host="0.0.0.0", port=5000, debug=True)