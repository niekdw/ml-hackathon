import csv
import json

csvfile = open('train_set.csv', encoding="utf8") 

fieldnames = ("label","approx_payout_date","body_length","channels","country","currency","delivery_method","description","email_domain","event_created","event_end","event_published","event_start","fb_published","gts","has_analytics","has_header","has_logo","listed","name","name_length","num_order","num_payouts","object_id","org_desc","org_facebook","org_name","org_twitter","payee_name","payout_type","previous_payouts","sale_duration","sale_duration2","show_map","ticket_types","user_age","user_created","user_type","venue_address","venue_country","venue_latitude","venue_longitude","venue_name","venue_state","sample_uuid")

reader = csv.DictReader( csvfile, fieldnames)


jsonfile = open('file.json', 'w')

for row in reader:
    json.dump(row, jsonfile)c
    jsonfile.write('\n')